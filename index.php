<?
$db = json_decode(file_get_contents('data.json'), JSON_OBJECT_AS_ARRAY);
$site = &$db[$_SERVER['HTTP_X_ORIGINAL_HOST']];
if (is_null($site['token'])) {
    $site['token'] = base64_encode(random_bytes(20));
}
if (is_null($site['password'])) {
    $site['password'] = base64_encode(random_bytes(8));
}
if (is_null($site['open_all'])) {
    $site['open_all'] = false;
}


$isAuth = (
    ($_SERVER['REMOTE_ADDR'] === '88.87.88.238')
    || in_array($_SERVER['REMOTE_ADDR'], $site['white_ips']) 
    || (
        $_COOKIE['ivauth'] && $site['token']
        && hash_equals($_COOKIE['ivauth'], $site['token']) 
    )
);

$siteAllow = ($site['open_all'] === true) || $isAuth;


if ($_SERVER['HTTP_IVAUTH']) {
    if (!$siteAllow) http_response_code(401);
    exit;
} 


if ($isAuth) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST['password']) {
            $site['password'] = $_POST['password'];
            $site['token'] = null;
        } else if ($_POST['ips']) {
            $site['white_ips'] = explode("\n", trim(str_replace("\r", '', $_POST['ips'])));
        } else if ($_POST['open_all']) {
            $site['open_all'] = !$site['open_all'];
        }

        header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
    } else {
        ?>
            <form method="post">
                <button type="submit" name='open_all' value="1">
                    <?=[true=>'Закрыть для всех', false=>'Открыть для всех'][$site['open_all']]?>
                </button>
            </form>
            <form method="post">
                <h2>current password: <?= $site['password'] ?></h2>
                <input name="password" placeholder="new password">
                <button type="submit">update</button>
            </form>
            <form method="post">
                <h2>white ip</h2>
                <textarea name="ips"><?=implode("\n", $site['white_ips'])?></textarea>
                <button type="submit">update</button>
            </form>
        <?
    }
} else {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST['password'] && hash_equals($_POST['password'], $site['password'])) {
            setcookie('ivauth',  $site['token']);
        }
        header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
    } else {
        http_response_code(401)
        ?>
            <form method="post">
                <input type="password" name="password" placeholder="need password"> <button type="submit">ok</button>
            </form>
        <?
    }
} 

file_put_contents('data.json', json_encode($db, JSON_PRETTY_PRINT), LOCK_EX);
