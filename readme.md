# Раздельная авторизация для ивсапорта



## в /etc/nginx/bx/conf/bitrix.conf
### внутрь location 
```
auth_request /ivauth-check;
```
### в конец
```
include iv/ivauth.conf;
```

## /etc/nginx/iv/ivauth.conf
```
location = /ivauth-check {
    proxy_pass http://127.0.0.1:8887;
    proxy_set_header	Host ivauth.ivsupport.ru;

    proxy_pass_request_body off;
    proxy_set_header        Content-Length "";
    
    proxy_set_header        X-Original-URI $request_uri;
    proxy_set_header	    X-Original-Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        ivauth true;

}

location = /ivauth {
    proxy_pass http://127.0.0.1:8887;
    proxy_set_header	Host ivauth.ivsupport.ru;

    proxy_set_header	X-Original-Host $host;
    proxy_set_header    X-Original-URI $request_uri;
    proxy_set_header	X-Real-IP $remote_addr;
    

}

error_page 401 = @login;
location @login {
    proxy_pass http://127.0.0.1:8887;
    proxy_set_header	Host ivauth.ivsupport.ru;

    proxy_set_header	X-Original-Host $host;
    proxy_set_header    X-Original-URI $request_uri;
    proxy_set_header	X-Real-IP $remote_addr;
}
```